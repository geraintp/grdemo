<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
if (  file_exists( dirname( __FILE__ ) . '/local-config.php' ) ) {
	include( dirname( __FILE__ ) . '/local-config.php' );
}else{
	// ** MySQL settings - You can get this info from your web host ** //
	/** The name of the database for WordPress */
	define('DB_NAME', $_SERVER["DB_NAME"] );

	/** MySQL database username */
	define('DB_USER', $_SERVER["DB_USER"] );

	/** MySQL database password */
	define('DB_PASSWORD', $_SERVER["DB_PASS"] );

	/** MySQL hostname */
	define('DB_HOST', $_SERVER["DB_HOST"] );

	/** Database Charset to use in creating database tables. */
	define('DB_CHARSET', 'utf8mb4');

	/** The Database Collate type. Don't change this if in doubt. */
	define('DB_COLLATE', '');

	/**
	 * WordPress Database Table prefix.
	 *
	 * You can have multiple installations in one database if you give each
	 * a unique prefix. Only numbers, letters, and underscores please!
	 */
	$table_prefix  = 'wp_';
}

// Move Content Dir out of wp folder
define ('WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'].'/content');
define ('WP_CONTENT_URL', 'http://'.$_SERVER['HTTP_HOST'].'/content');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'g&o?_#=5y?_`-PZ!}q.reUsIj6?g7CpRhgS|^uzNH_P:VtY0]w?0Uyz:@Q2~6&,u');
define('SECURE_AUTH_KEY',  '.PC,W7m&l>oYa;f]LgYCNH{SN}_{z7V10,}{u}GVhJ(,rD7(~[`*<zgQA!?S,9{_');
define('LOGGED_IN_KEY',    ')q]?F{pp>iLHbl5M-~MVuSQGSn[o;aQZ9pay$:`a^S5OGxd>D 43#fNE1=tB2$&d');
define('NONCE_KEY',        'em3Hwd(AARN*rRj6n=tIG1)4}Rsn?q9<iY-N*bt{T}VuSTR:piG>PT/b&bp9QM3g');
define('AUTH_SALT',        'hgC;P/U.NRTu:wXr(Dy0XIGBKR6;V2gGMZvj>C=dHJqBAx Rv<LpJdI>9?.kshdy');
define('SECURE_AUTH_SALT', '`<W:kHq/O|ih*,kYB}ju3[zBC{V;N#t+:$WP-W~+791Ve5Ybj`(CS`dZzq7oMb:s');
define('LOGGED_IN_SALT',   'QONP=Pgr,kMv_;&)<~#w+KzS=&rLgc?a8Pu$]X9b%6c@WB}T$8l35Q8SG6BlzlzP');
define('NONCE_SALT',       '<n]BQb0ikAl0Eo-[x2nFkFt)sn+}kQh#gSX2*+x.K}}uz=fF(@0DB*@lER%1*Bpm');

/**#@-*/

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
if ( !defined('WP_DEBUG') )
	define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
