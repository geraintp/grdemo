/* gulpfile.js */
var 
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps');

// source and distribution folder
var
    source = 'src/',
    dest = './';

// Bootstrap scss source
var bootstrapSass = {
    in: './node_modules/bootstrap-sass/'
};

// fonts
var fonts = {
    in: [source + 'fonts/*.*', bootstrapSass.in + 'assets/fonts/**/*'],
    out: dest + 'fonts/'
};

// JavaScript
var js = {
    in: [source + 'js/*.*', bootstrapSass.in + 'assets/javascripts/*.*'],
    out: dest + 'js/',
    watch: source + 'js/*.*'
};

 // css source file: .scss files
var css = {
    in: source + 'scss/style.scss',
    out: dest + 'css/',
    watch: source + 'scss/**/*',
    sassOpts: {
        outputStyle: 'expanded',
        precision: 8,
        errLogToConsole: true,
        includePaths: [bootstrapSass.in + 'assets/stylesheets']
    }
};

gulp.task('fonts', function () {
    return gulp
        .src(fonts.in)
        .pipe(gulp.dest(fonts.out));
});

gulp.task('js', function () {
    return gulp
        .src(js.in)
        .pipe(gulp.dest(js.out));
});

// compile scss
gulp.task('sass', ['fonts'], function () {
    return gulp.src(css.in)
        .pipe(sourcemaps.init())
        .pipe(sass(css.sassOpts))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(css.out));
});

// compile scss
gulp.task('production', ['fonts'], function () {
    css.sassOpts.outputStyle = 'compressed';
    return gulp.src(css.in)
        .pipe(sourcemaps.init())
        .pipe(sass(css.sassOpts))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(css.out));
});

// default task
gulp.task('default', ['sass', 'js'], function () {
     gulp.watch(css.watch, ['sass']);
     gulp.watch(js.watch, ['js']);
});