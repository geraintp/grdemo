<?php defined('ABSPATH') or die('not found') ?>
	<span id="top-link-block" class="hidden">
	    <a href="#top" class="well well-lg" onclick="jQuery('html,body').animate({scrollTop:0},'500');return false;">
	        <i class="glyphicon glyphicon-chevron-up"></i> 
	    </a>
	</span><!-- /top-link-block -->
	<?php wp_footer() ?>	
	<script type="text/javascript">
		// Only enable if the document has a long scroll bar
		jQuery(document).ready(function ($) {
			// Note the window height + offset
			if ( ($(window).height() + 100) < $(document).height() ) {
			    $('#top-link-block').removeClass('hidden').affix({
			        offset: {top:100}
			    });
			}
		});
	</script>
	</body>
</html>