<?php defined('ABSPATH') or die('not found');
/**
 * Theme Class to Namespace and manage theme functions
 * Template Name: Contact V2
 */
get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<section class="container page-title">
			<div class="row">
				<div class="col-md-16">
					<div class="jumbotron">
						<h1><?php the_title() ?></h1>
					 	<?php the_content() ?>
					</div>
				</div>
			</div>
		</section>
		<?php 
		$data = get_fields();	
		foreach ( $data['sections'] as $section ) {
				switch ($section['section']) {
					case 'contact':
						include(locate_template('partials/team-contacts.php'));
						break;

					case 'jobs':
						include(locate_template('partials/team-vacancies.php'));
						break;

					case 'history':
						include(locate_template('partials/team-story.php'));
						break;
					
					default:
						echo $name; 
						break;
				}
		}			
		?>
<!-- 		<section>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-sx-12">&nbsp;<br>&nbsp;</div>	
			</div>
		</section> -->
<?php endwhile; else : ?>
	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
<?php get_footer(); ?>