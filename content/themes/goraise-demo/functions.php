<?php 
defined('ABSPATH') or die('not found');
/**
 * Theme Class to Namespace and manage theme functions
 */
 class GoRaiseTheme
 {
 	private $_instance = null; 

 	/**
 	 * Singleton Accessor.
 	 *
 	 * @return Object GoRaiseTheme 
 	 **/
 	public static function init()
 	{
 		if ( !isset( self::$_instance ) ) {
 			return new self();
 		}
 		return self::$_instance;
 	}

 	/**
 	 * Theme Constructor, Sets up theme adding actions & filters
 	 *
 	 * @return void
 	 **/
 	private function __construct()
 	{
 		add_action('wp_enqueue_scripts', [ $this, 'enqueue_styles' ] );
 		add_action('wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
 	}

 	public function enqueue_scripts()
 	{
 		wp_enqueue_script('go-raise-demo', get_template_directory_uri().'/js/bootstrap.min.js', ['jquery'], 
 			null, true );
 		wp_enqueue_script('go-raise-typekit', 'https://use.typekit.net/zld8qai.js', [], null );
 		wp_add_inline_script( 'go-raise-typekit', '<script>try{Typekit.load({ async: true });}catch(e){}</script>' );
 	}

 	public function enqueue_styles()
 	{
 		// Load the main stylesheet
    	wp_enqueue_style( 'go-raise-demo', get_template_directory_uri().'/css/style.css' );
 	}
 } 

GoRaiseTheme::init();
?>