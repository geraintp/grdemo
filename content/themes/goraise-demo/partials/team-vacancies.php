		<div class="full vacancies-bg" <?php echo get_field('op_background_image') ? 'style="background-image:url('. $data['op_background_image'] .')"': ''; ?>>
		<section class="container vacancies">
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12 section-lead">
					<h2><?php echo $data['op_title'] ?></h2>
					<?php if ( !empty( $data['op_description'] )): ?>
						<p><?php echo $data['op_description'] ?></p>
					<?php endif ?>
					<?php if ( !empty( $data['op_button_link_url'] ) ): ?>
						<a href="<?php echo ( $value = get_field( "op_button_link_url" ) ) ? $value : 'Learn more';?>" class="btn btn-white"><?php echo ( $value = get_field( "op_button_link_text" ) ) ? $value : 'Learn more';?></a>
					<?php endif ?>
				</div>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<?php $i = 1; 
					if (isset($data['op_positions'])):  ?>
						<?php foreach ( $data['op_positions'] as $panel ): ?>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading-<?php echo $i ?>">
								<h4 class="panel-title">
									<a class="<?php echo $i==1 ? '':'collapsed' ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $i ?>" aria-expanded="true" aria-controls="collapse-<?php echo $i ?>">
									<?php echo $panel['role_title'] ?>
									</a>
								</h4>
							</div>
							<div id="collapse-<?php echo $i ?>" class="panel-collapse collapse <?php echo $i==1 ? 'in':'' ?>" role="tabpanel" aria-labelledby="heading-<?php echo $i++ ?>">
								<div class="panel-body">
								<p><?php echo $panel['role_description'] ?></p>
								<?php if ( !empty( $panel['link_url'] ) ): ?>
									<a href="<?php echo $panel['link_url'] ?>" class="btn btn-primary"><?php echo $panel['link_text'] ?></a>	
								<?php endif ?>
								</div>
							</div>
						</div>
						<?php endforeach ?>
					<?php else: ?>

					<?php endif ?>
					</div>
				</div>
			</div>
		</section>
		</div>