		<section class="container staff-cards">
			<div class="row">
				<?php foreach ( $data['staff_cards'] as $staff ): ?>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="panel panel-default staff-card">
						<div class="panel-body">
							<?php if ( !empty( $staff['avatar'] ) ): ?>
								<img src="<?php echo $staff['avatar'] ?>" class="img-circle" alt="GoRaise">
							<?php else: ?>
								<img src="http://via.placeholder.com/160x160/E6176D/ffffff?text=GR" class="img-circle" alt="GoRaise">
							<?php endif ?>
						
							<?php if ( !empty( $staff['name'] )): ?>
								<h3><?php echo $staff['name'] ?></h3>
							<?php endif ?>
							<?php if ( !empty( $staff['title'] )): ?>
								<h4><?php echo $staff['title'] ?></h4>
							<?php endif ?>
							<?php if ( !empty( $staff['description'] )): ?>
								<p><?php echo $staff['description'] ?></p>
							<?php endif ?>
						</div>
					</div>
				</div>
				<?php endforeach ?>
		 	</div>
		</section>