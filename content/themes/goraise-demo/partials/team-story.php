		<section class="container story">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12 section-lead">
					<h2><?php echo $data['os_title'] ?></h2>
					<p><?php echo $data['os_description'] ?></p>

					<a href="<?php echo ( $value = get_field( "os_button_link_url" ) ) ? $value : 'Learn more';?>" class="btn btn-primary"><?php echo ( $value = get_field( "os_button_link_text" ) ) ? $value : 'Learn more';?></a>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 gallery">
					<ul>
						<li>
							<?php if ($data['os_first_image']): ?>
								<img src="<?php echo $data['os_first_image'] ?>">
							<?php else: ?>
								<img src="<?php echo get_template_directory_uri() ?>/img/creative-2.jpg">
							<?php endif ?>		
						</li>
						<li>
							<?php if ($data['os_second_image']): ?>
								<img src="<?php echo $data['os_first_image'] ?>">
							<?php else: ?>
								<img src="<?php echo get_template_directory_uri() ?>/img/creative-1.jpg">
							<?php endif ?>	
						</li>
						<li>
							<?php if ($data['os_third_image']): ?>
								<img src="<?php echo $data['os_first_image'] ?>">
							<?php else: ?>
								<img src="<?php echo get_template_directory_uri() ?>/img/creative-3.jpg">
							<?php endif ?>	
						</li>
					</ul>
				</div>
 			</div>
 		</section>
